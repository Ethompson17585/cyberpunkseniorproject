﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Hint : MonoBehaviour
{
    [SerializeField] GameObject player;

    // Update is called once per frame
    void Update()
    {
        SetText();
    }

    private void SetText()
    {
        TextMeshProUGUI HintText = GetComponent<TextMeshProUGUI>();
        if(player.transform.position.x <= 0.5f)
        {
            HintText.SetText("HINT: Use SPACE to jump.");
        }
        else if (player.transform.position.x > 0.5f && player.transform.position.x <= 11)
        {
            HintText.SetText("HINT: Use W to climb the pole.");
        }
        else if (player.transform.position.x > 11 && player.transform.position.x <= 18)
        {
            HintText.SetText("HINT: Use E next to the terminal to teleport.");
        }
        else if (player.transform.position.x > 18 && player.transform.position.x <= 34)
        {
            HintText.SetText("HINT: Use SPACE to jump into the next building.");
        }
        else if (player.transform.position.x > 34 && player.transform.position.x <= 44)
        {
            HintText.SetText("HINT: Use S to climb down the ladders.");
        }
        else if (player.transform.position.x > 44 && player.transform.position.x <= 62 && player.transform.position.y < 4)
        {
            HintText.SetText("HINT: Use E next to the terminal to teleport.");
        }
        else if (player.transform.position.x > 11 && player.transform.position.x <= 62 && player.transform.position.y > 4)
        {
            HintText.SetText("HINT: Use W to climb the pole.");
        }
        else if (player.transform.position.x > 62)
        {
            HintText.SetText("HINT: Jump off the ledge.");
        }
    }
}
