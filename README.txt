#Owners: 
#-Ethan Thompson
#-Levi Bevins
#-Gavin Feehan

#Description:
#-This is where all of the files are stored for our game.

#Recent additions:
#-Demo tiles
#-Demo sprites
#-Final unity project
#-testing_world unity project
#-README.txt edits
#-HOWTO edits

#Author Notes:
#-Please be careful when making edits!!!
#-For any questions, contact Ethan Thompson.
#-Demo tiles and sprites will function for the creation #of the game mechanics while #better tiles and sprites are #created.