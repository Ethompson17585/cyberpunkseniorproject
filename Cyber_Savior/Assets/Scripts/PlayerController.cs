﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float runSpeed = 5f;
    [SerializeField] float jumpSpeed = 5f;
    [SerializeField] float climbSpeed = 5f;
    bool isAlive = true;
    bool maskActive = false;
    Animator myAnimator;
    Rigidbody2D myRigidBody;
    CapsuleCollider2D myBodyCollider;
    BoxCollider2D myFeet;
    float gravityScaleAtStart;
    // Start is called before the first frame update
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        myBodyCollider = GetComponent<CapsuleCollider2D>();
        myFeet = GetComponent<BoxCollider2D>();
        gravityScaleAtStart = myRigidBody.gravityScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isAlive) { return; }
        Run();
        Jump();
        enableMask();
        FlipSprite();
        ClimbLadder();
       // Die();
       // Teleport();
       // Hint();
    }

    private void enableMask()
    {

        if (Input.GetKeyDown(KeyCode.Q))
        {
            maskActive = !maskActive;
            //if (maskActive == true)
            //{
            //    //myAnimator.SetBool("MaskDown", true);
            //}
            //else
            //{
            //    //myAnimator.SetBool("MaskUp", true);
            //}

        }
    }
    private void Run()
    {
        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;
        float controlThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        Vector2 playerVelocity = new Vector2(controlThrow * runSpeed, myRigidBody.velocity.y);
        myRigidBody.velocity = playerVelocity;
        //myAnimator.SetBool("Walk", playerHasHorizontalSpeed);
        if (maskActive == true)
        {
            if (playerHasHorizontalSpeed == false)
            {
                myAnimator.SetBool("IdleMask", true);
                myAnimator.SetBool("WalkMask", false);
                myAnimator.SetBool("Running", false);
                Debug.Log("mask:" + maskActive);
                Debug.Log("player has speed " + playerHasHorizontalSpeed);
            }
            if (playerHasHorizontalSpeed == true)
            {
                myAnimator.SetBool("WalkMask", true);
                myAnimator.SetBool("IdleMask", false);
                myAnimator.SetBool("Running", false);
                Debug.Log("mask:" + maskActive);
                Debug.Log("player has speed " + playerHasHorizontalSpeed);
            }
        }
        if (maskActive == false)
        {
            if (playerHasHorizontalSpeed == true)
            {
                myAnimator.SetBool("Walk", true);
            }
            if (playerHasHorizontalSpeed == false)
            {
                myAnimator.SetBool("Walk", false);
            }

        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            runSpeed = 4.5f;
            maskActive = true;
            myAnimator.SetBool("Running", playerHasHorizontalSpeed);
            //Debug.Log(playerHasHorizontalSpeed);
        }
        else
        {
            runSpeed = 3.5f;
        }
    }
    private void ClimbLadder()
    {
        if (!myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Climbing")))
        {
            //myAnimator.SetBool("Climbing", false);
            myRigidBody.gravityScale = gravityScaleAtStart;
            return;
        }
        if (!CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            float controlThrow = CrossPlatformInputManager.GetAxis("Vertical");
            Vector2 climbVelocity = new Vector2(myRigidBody.velocity.x, controlThrow * climbSpeed);
            myRigidBody.velocity = climbVelocity;
            myRigidBody.gravityScale = 0f;
            bool playerHasVerticalSpeed = Mathf.Abs(myRigidBody.velocity.y) > Mathf.Epsilon;
        }



        //myAnimator.SetBool("Climbing", playerHasVerticalSpeed);
    }
    private void Jump()
    {
        if (!myFeet.IsTouchingLayers(LayerMask.GetMask("Foreground")) && !myFeet.IsTouchingLayers(LayerMask.GetMask("Climbing")))
        {
            return;
        }
        else if (CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            Vector2 jumpVelocityToAdd = new Vector2(0f, jumpSpeed);
            myRigidBody.velocity += jumpVelocityToAdd;
        }
    }
    private void Die()
    {
        if (myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Enemy", "Hazards")))
        {
            isAlive = false;
            myAnimator.SetTrigger("Die");
           // GetComponent<Rigidbody2D>().velocity = deathKick;
        }

        if (myRigidBody.velocity.y <= -16)
        {
            transform.position = new Vector2(-2, -3.4f);
            myRigidBody.velocity = Vector2.zero;
        }
    }
    private void FlipSprite()
    {
        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;
        if (playerHasHorizontalSpeed)
        {
            transform.localScale = new Vector2(Mathf.Sign(myRigidBody.velocity.x), 1f);
        }
    }

    private void Teleport()
    {
        if (myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Teleporter1")) && Input.GetKeyDown(KeyCode.E))
        {
           // transform.position = new Vector2(Teleporter2.transform.position.x + 1, Teleporter2.transform.position.y);
           // ps.Play();
        }
        else if (myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Teleporter2")) && Input.GetKeyDown(KeyCode.E))
        {
           // transform.position = new Vector2(Teleporter1.transform.position.x - 1, Teleporter1.transform.position.y);
           // ps.Play();
        }
        else if (myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Teleporter3")) && Input.GetKeyDown(KeyCode.E))
        {
           // transform.position = new Vector2(Teleporter4.transform.position.x + 1, Teleporter4.transform.position.y);
           // ps.Play();
        }
        else if (myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Teleporter4")) && Input.GetKeyDown(KeyCode.E))
        {
            //transform.position = new Vector2(Teleporter3.transform.position.x - 1, Teleporter3.transform.position.y);
            //ps.Play();
        }
        else
        {
            return;
        }
    }

    private void Hint()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            //HintText.SetActive(true);
        }
        if (Input.GetKeyUp(KeyCode.H))
        {
            //HintText.SetActive(false);
        }
    }
}
